=== Wordpress Downloads by Cyber Sprocket Labs  ===
Contributors: cybersprocket
Tags: ecommerce, e-commerce, commerce, woothemes, wordpress ecommerce, store, sales, sell, shop, shopping, cart, checkout, configurable, variable, download, downloadable, digital
Donate link: https://www.paypal.com/cgi-bin/webscr?cmd=_xclick&business=paypal@cybersprocket.com&item_name=Donation+for+WooCommerce+Licensing
Requires at least: 3.3
Tested up to: 3.4
Stable tag: 0.1
License: GPLv3
License URI: http://www.gnu.org/licenses/gpl-3.0.html

Track realtime downloads of wordpress plugins

== Description ==

See your download stats in realtime

== Installation ==

= Minimum Requirements =

* WordPress 3.3 or greater
* PHP version 5.2.4 or greater
* MySQL version 5.0 or greater
* Woocommerce
* Paypal payment gateway

= Automatic installation =

Automatic installation is the easiest option as WordPress handles the file transfers itself and you don�t even need to leave your web browser. To do an automatic install of WooCommerce, log in to your WordPress admin panel, navigate to the Plugins menu and click Add New. 

In the search field type �WooCommerce License� and click Search Plugins. Once you�ve found our eCommerce plugin you can view details about it such as the the point release, rating and description. Most importantly of course, you can install it by simply clicking Install Now. After clicking that link you will be asked if you�re sure you want to install the plugin. Click yes and WordPress will automatically complete the installation. 

= Manual installation =

The manual installation method involves downloading our eCommerce plugin and uploading it to your webserver via your favourite FTP application.

1. Download the plugin file to your computer and unzip it
2. Using an FTP program, or your hosting control panel, upload the unzipped plugin folder to your WordPress installation�s wp-content/plugins/ directory.
3. Activate the plugin from the Plugins menu within the WordPress admin.

= Upgrading =

Automatic updates should work a charm; as always though, ensure you backup your site just in case. 

If on the off chance you do encounter issues with the shop/category pages after an update you simply need to flush the permalinks by going to WordPress > Settings > Permalinks and hitting 'save'. That should return things to normal.

== Frequently Asked Questions == 

= Questions?  What Questions? =

This plugin is meant to provide a simple way to return a license key back to the user.   This is used for all Cyber Sprocket Freemium plugins.  It returns a license key back for any product that has [Transaction ID] in the product notes field.

== Screenshots ==

1. None

== Changelog ==

= 0.1 - June 2012 =
* Initial Release
