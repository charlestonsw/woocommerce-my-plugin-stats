<?php
/**
 * Plugin Name: Wordpress Downloads by Cyber Sprocket Labs
 * Plugin URI: http://www.cybersprocket.com/products/woolicense/
 * Description: Track realtime downloads of wordpress plugins
 * Version: 0.1
 * Author: Cyber Sprocket Labs
 * Author URI: http://cybersprocket.com
 * Requires at least: 3.3
 * Tested up to: 3.4
 * 
 * Text Domain: woocommerce
 * Domain Path: /languages/
 * 
 * @package WooCommerce
 * @category LicenseHooks
 * @author Cyber Sprocket Labs
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

// No Woocommerce? Get out...
//
if ( !in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
 return;
}

if ( ! class_exists( 'WoocommercePluginDownloads' ) ) {

add_filter('woocommerce_reports_charts', array('WoocommercePluginDownloads', 'track'));

function plugin_dl_overview() {
    global $start_date, $end_date, $woocommerce, $wpdb, $wp_locale;
	
	$start_date = (isset($_POST['start_date'])) ? $_POST['start_date'] : '';
	$end_date	= (isset($_POST['end_date'])) ? $_POST['end_date'] : '';
	
	if (!$start_date) $start_date = date('Ymd', strtotime( date('Ym', current_time('timestamp', true)).'01' ));
	if (!$end_date) $end_date = date('Ymd', current_time('timestamp', true));
	
	$start_date = strtotime($start_date);
	$end_date = strtotime($end_date);
	
	$total_sales = 0;
	$total_orders = 0;
	$order_items = 0;

    //http get the json
    $url = 'http://wpapi.org/api/plugin/store-locator-le.json';

    $json = json_decode(file_get_contents($url), true);
    
    $stats = $json['stats'];
    
    $count_stats = array();
    $count_total = array();

	// Blank date ranges to begin
	$count = 0;
	$days = ($end_date - $start_date) / (60 * 60 * 24);
	if ($days==0) $days = 1;

    $total_downloads_range = 0;
    $total_downloads = 0;
    $time = $start_date;

	while ($count < $days) :
        $time += 86400;
        $lookup = date('Y-m-d', $time);

        $count_stats[$time] = $stats[$lookup];
        $total_downloads_range += $stats[$lookup];

		$count++;
	endwhile;

    $count = 0;
    $lower_range_total = -1;
    $upper_range_total = -1;

    foreach ($stats as $stat_date => $stat) :
        $total_downloads += $stat;
        $time = strtotime($stat_date);

        if ($time >= $start_date) {
            if ($lower_range_total == -1) $lower_range_total = $total_downloads;
            $count_total[$time] = $total_downloads;
        }

        $count++;
    endforeach;

    $upper_range_total = $total_downloads;
    $breakup = $upper_range_total - $lower_range_total / 10;
    $average_downloads_per_day = $total_downloads / $count;

    ?>
	<form method="post" action="">
		<p><label for="from"><?php _e('From:', 'woocommerce'); ?></label> <input type="text" name="start_date" id="from" readonly="readonly" value="<?php echo esc_attr( date('Y-m-d', $start_date) ); ?>" /> <label for="to"><?php _e('To:', 'woocommerce'); ?></label> <input type="text" name="end_date" id="to" readonly="readonly" value="<?php echo esc_attr( date('Y-m-d', $end_date) ); ?>" /> <input type="submit" class="button" value="<?php _e('Show', 'woocommerce'); ?>" /></p>
	</form>
	
	<div id="poststuff" class="woocommerce-reports-wrap">
		<div class="woocommerce-reports-sidebar">
			<div class="postbox">
				<h3><span><?php _e('Total Downloads in range', 'woocommerce'); ?></span></h3>
				<div class="inside">
					<p class="stat"><?php if ($total_downloads_range>0) echo $total_downloads_range; else _e('n/a', 'woocommerce'); ?></p>
				</div>
			</div>
			<div class="postbox">
				<h3><span><?php _e('Total downloads to date', 'woocommerce'); ?></span></h3>
				<div class="inside">
					<p class="stat"><?php if ( $total_downloads > 0 ) echo $total_downloads; else _e('n/a', 'woocommerce'); ?></p>
				</div>
			</div>
			<div class="postbox">
				<h3><span><?php _e('Average downloads per day over range', 'woocommerce'); ?></span></h3>
				<div class="inside">
					<p class="stat"><?php if ($total_downloads_range>0) echo number_format($total_downloads_range/$days, 2); else _e('n/a', 'woocommerce'); ?></p>
				</div>
			</div>
			<div class="postbox">
				<h3><span><?php _e('Average downloads per day', 'woocommerce'); ?></span></h3>
				<div class="inside">
					<p class="stat"><?php if ($average_downloads_per_day>0) echo number_format($average_downloads_per_day, 2); else _e('n/a', 'woocommerce'); ?></p>
				</div>
			</div>
		</div>
		<div class="woocommerce-reports-main">
			<div class="postbox">
				<h3><span><?php _e('Downloads per day in range', 'woocommerce'); ?></span></h3>
				<div class="inside chart">
					<div id="placeholder" style="width:100%; overflow:hidden; height:568px; position:relative;"></div>
				</div>
			</div>
		</div>
	</div>
	<?php

    $download_counts_array = array();
    $count_total_array = array();
	foreach ($count_stats as $key => $count) :
		$download_counts_array[] = array($key * 1000, $count);
	endforeach;

    foreach ($count_total as $key => $count) :
        $count_total_array[] = array($key * 1000, $count);
    endforeach;

	$download_data = array( 'order_counts' => $download_counts_array, 'total_counts' => $count_total_array);

    $chart_data = json_encode($download_data);
	?>
	<script type="text/javascript">
		jQuery(function(){
			var order_data = jQuery.parseJSON( '<?php echo $chart_data; ?>' );
			var d = order_data.order_counts;
            var d2 = order_data.total_counts;
			
			for (var i = 0; i < d.length; ++i) d[i][0] += 60 * 60 * 1000;
            for (var i = 0; i < d2.length; ++i) d2[i][0] += 60 * 60 * 1000;
			
			var placeholder = jQuery("#placeholder");
			 
			var plot = jQuery.plot(placeholder, [ { label: "<?php echo esc_js( __( 'Number of downloads', 'woocommerce' ) ) ?>", data: d }, { label: "<?php echo esc_js( __( 'Total number of downloads', 'woocommerce' ) ) ?>", data: d2, yaxis: 2  }], {
				series: {
					lines: { show: true },
					points: { show: true }
				},
				grid: {
					show: true,
					aboveData: false,
					color: '#ccc',
					backgroundColor: '#fff',
					borderWidth: 2,
					borderColor: '#ccc',
					clickable: false,
					hoverable: true,
					markings: weekendAreas
				},
				xaxis: { 
					mode: "time",
					timeformat: "%d %b", 
					monthNames: <?php echo json_encode( array_values( $wp_locale->month_abbrev ) ) ?>,
					tickLength: 1,
					minTickSize: [1, "day"]
				},
				yaxes: [ { min: 0, tickSize: 10, tickDecimals: 0 }, { position: 'right', tickSize: '<?php $breakup ?>', tickDecimals: 0 }],
		   		colors: ["#8a4b75", "#47a03e"]
		 	});
		 	
		 	placeholder.resize();
	 	
			<?php woocommerce_weekend_area_js(); ?>
			<?php woocommerce_tooltip_js(); ?>
			<?php woocommerce_datepicker_js(); ?>
		});
	</script>
	<?php
}

/**
 * Main WooCommerce Class
 *
 * Contains the main functions for WooCommerce, stores variables, and handles error messages
 *
 * @since WooCommerce 1.4
 */
class WoocommercePluginDownloads {
	
	/**
	 * Constructor
	 *
	 * Gets things started
	 */
	function __construct() {
	}

    

    /**
	 * Look up license information and tell the caller if they are good or not
	 **/
    function track($charts) {
        $charts['downloads'] = array (
            'title' => __('WP Downloads', 'csl-downloads'),
            'charts' => array(
                array(
                    'title' => 'Overview',
                    'description' => 'All downloads',
                    'function' => 'plugin_dl_overview'
                ),
            )
        );

        return $charts;
    }

    
}

} // class_exists check
